import os, sys
LDAPSS_PROJECT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
LDAPSS_PATH = os.path.abspath(os.path.dirname(__file__))
# print LDAPSS_PROJECT
# print LDAPSS_PATH
sys.path.append(LDAPSS_PROJECT)
sys.path.append(LDAPSS_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = 'recetario.settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
